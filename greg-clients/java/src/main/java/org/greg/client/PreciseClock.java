package org.greg.client;

class PreciseClock {
    private static final long utcOrigin = System.currentTimeMillis();
    private static long originNano = System.nanoTime();

    public static final PreciseClock INSTANCE = new PreciseClock();

    public long now() {
        return 1000000L * utcOrigin + (System.nanoTime() - originNano);
    }
}
