package org.greg.server;

import java.io.*;
import java.net.ServerSocket;
import java.net.Socket;
import java.net.SocketAddress;
import java.util.*;
import java.util.concurrent.*;
import java.util.concurrent.atomic.AtomicInteger;
import java.util.zip.GZIPInputStream;

public class GregServer {

    private static String get(String[] args, String arg, String def) {
        for (int i = 0; i < args.length - 1; ++i) {
            if (args[i].equals("-" + arg))
                return args[i + 1];
        }
        return def;
    }

    private static int get(String[] args, String arg, int def) {
        return Integer.parseInt(get(args, arg, "" + def));
    }


    public static void main(String[] args) {

        if(Boolean.parseBoolean(get(args, "verbose", "false"))) {
            Trace.ENABLED = true;
        }

        Trace.writeLine("GregServer started");

        Configuration conf = new Configuration();
        conf.messagePort = get(args, "port", 5676);
        conf.calibrationPort = get(args, "calibrationPort", 5677);
        conf.desiredConfidenceRangeMs = get(args, "confidenceRangeMs", 1);
        conf.maxCalibrationIters = get(args, "maxCalibrationIters", 100);
        conf.minCalibrationIters = get(args, "minCalibrationIters", 10);
        conf.preCalibrationIters = get(args, "preCalibrationIters", 10);
        conf.maxPendingCalibrated = get(args, "maxPendingCalibrated", 1000000);
        conf.maxPendingUncalibrated = get(args, "maxPendingUncalibrated", 100000);
        conf.timeWindowSec = get(args, "timeWindowSec", 5);

        final GregServer server = new GregServer(conf);

        new Thread() {
            public void run() {
                server.acceptMessages();
            }
        }.start();
        new Thread() {
            public void run() {
                server.acceptCalibration();
            }
        }.start();
        new Thread() {
            public void run() {
                server.flushCalibratedMessages();
            }
        }.start();

        new Thread() {
            public void run() {
                try {
                    Thread.currentThread().setPriority(7); // Above normal

                    OutputStream os = new BufferedOutputStream(new FileOutputStream(FileDescriptor.out), 16384);

                    while (true) {
                        List<Record> records = server.outputQueue.dequeue();

                        if (records.isEmpty()) {
                            Thread.sleep(50);
                            continue;
                        }

                        for (Record rec : records) {
                            ClientInfo ci = server.clientInfo.get(rec.uuid);
                            os.write(ci.host);
                            os.write(' ');
                            // os.write(ci.clientId);
                            // os.write(' ');
                            os.write(DateTimeFormatter.toBytes(rec.timestamp));
                            os.write(' ');
                            os.write(rec.message);
                        }

                        os.flush();
                    }
                } catch (Exception e) {
                    Trace.writeLine("Failure while writing records", e);
                }
            }
        }.start();
    }

    private final Configuration conf;
    private final TimeBufferedQueue outputQueue;

    private final ConcurrentMap<UUID, Queue<Record>> clientRecords = new ConcurrentHashMap<UUID, Queue<Record>>();
    private final ConcurrentMap<UUID, Long> clientLateness = new ConcurrentHashMap<UUID, Long>();
    private final ConcurrentMap<UUID, ClientInfo> clientInfo = new ConcurrentHashMap<UUID, ClientInfo>();
    private final AtomicInteger numPendingUncalibratedEntries = new AtomicInteger(0);

    public GregServer(Configuration conf) {
        this.conf = conf;

        Comparator<Record> recordsComparator = new Comparator<Record>() {
            public int compare(Record x, Record y) {
                if(x.timestamp < y.timestamp)
                    return -1;
                if(x.timestamp > y.timestamp)
                    return 1;
                return 0;
            }
        };

        this.outputQueue = new TimeBufferedQueue(1000000000L * conf.timeWindowSec,
                                                 PreciseClock.INSTANCE,
                                                 conf.maxPendingCalibrated,
                                                 recordsComparator);
    }


    private void acceptMessages() {
        ServerSocket server = null;
        try {
            server = new ServerSocket(conf.messagePort);
        } catch (IOException e) {
            Trace.writeLine("Cannot create messages acceptor", e);
        }


        while (true) {
            final Socket client;
            final InputStream stream;
            try {
                client = server.accept();
                stream = client.getInputStream();
            } catch (Exception e) {
                throw new RuntimeException("Failed to accept client or get its input stream", e);
            }

            new Thread() {
                public void run() {
                    try {
                        processRecordsBatch(stream, client.getRemoteSocketAddress());
                    } finally {
                        try {
                            client.close();
                        } catch (Exception e) {
                            // Ignore
                        }
                    }
                }
            }.start();
        }
    }

    private void processRecordsBatch(InputStream rawStream, final SocketAddress ep) {
        try {
            InputStream stream = new BufferedInputStream(rawStream, 65536);

            DataInput r = new LittleEndianDataInputStream(stream);
            final UUID uuid = new UUID(r.readLong(), r.readLong());


            int cidLenBytes = r.readInt();
            byte[] cidBytes = new byte[cidLenBytes];
            r.readFully(cidBytes);

            int machineLenBytes = r.readInt();
            byte[] machineBytes = new byte[machineLenBytes];
            r.readFully(machineBytes);

            clientInfo.put(uuid, new ClientInfo(cidBytes, machineBytes));

            boolean useCompression = r.readBoolean();

            final InputStream messagesStream = useCompression ? new GZIPInputStream(stream) : stream;

            clientRecords.putIfAbsent(uuid, new ConcurrentLinkedQueue<Record>());
            final Queue<Record> q = clientRecords.get(uuid);

            while(true) {

                final boolean[] skipping = new boolean[] {false};
                final int[] numRead = {0};
                final int[] numSkipped = {0};

                final List<Record> uncalibrated = new ArrayList<Record>();
                final List<Record> calibrated = new ArrayList<Record>();

                final Long lateness = clientLateness.get(uuid);

                Sink<Record> sink = new Sink<Record>() {
                    public void consume(Record rec) {
                        numRead[0]++;

                        if(lateness == null) {
                            int numPending = numPendingUncalibratedEntries.incrementAndGet();
                            if (numPending < conf.maxPendingUncalibrated) {
                                uncalibrated.add(rec);

                                if (skipping[0]) {
                                    Trace.writeLine("Receiving entries from client " + ep +
                                                    " again, after having skipped " + numSkipped[0]);
                                }
                                skipping[0] = false;
                                numSkipped[0] = 0;
                            } else {
                                numPendingUncalibratedEntries.decrementAndGet();

                                numSkipped[0]++;
                                if (!skipping[0] || numSkipped[0] % 10000 == 0) {
                                    Trace.writeLine(
                                            "Uncalibrated records buffer full - skipping entry from client " + ep +
                                                    " because there are already " + numPending + " uncalibrated entries. " +
                                                    (numSkipped[0] == 1 ? "" : (numSkipped[0] + " skipped in a row...")));
                                }
                                skipping[0] = true;
                            }
                        } else {
                            calibrated.add(rec.absolutizeTime(lateness));
                        }
                    }
                };

                readRecords(uuid, messagesStream, sink);

                // Only publish records to main queue if we read all them successfully (had no exception to this point)
                // Otherwise we'd have duplicates if clients resubmit their records after failure.
                q.addAll(uncalibrated);
                outputQueue.enqueue(calibrated);

                if (skipping[0]) {
                    Trace.writeLine("Skipped " + numSkipped[0] + " entries from " + ep + " in a row.");
                }

                Trace.writeLine("Read " + numRead[0] + " entries");

            }
        } catch (Exception e) {// Socket or IO or whatever
            Trace.writeLine("Failed to receive records batch, ignoring", e);
            // Ignore
        }
    }

    private interface Sink<T> {
        void consume(T t);
    }

    private void readRecords(UUID uuid, InputStream stream, Sink<Record> sink) throws IOException {
        DataInput r = new LittleEndianDataInputStream(new BufferedInputStream(stream, 65536));

        Clock c = PreciseClock.INSTANCE;

        while (true) {
            long timestamp = r.readLong();
            if(timestamp == 0) break;

            int msgLenBytes = r.readInt();
            byte[] msgBytes = new byte[msgLenBytes];
            r.readFully(msgBytes);

            sink.consume(new Record(uuid, c.now(), timestamp, msgBytes));
        }
    }

    private void acceptCalibration() {
        ServerSocket calibrationServer;
        try {
            calibrationServer = new ServerSocket(conf.calibrationPort);
        } catch (IOException e) {
            throw new RuntimeException("Failed to create calibration listener", e);
        }

        Executor executor = Executors.newFixedThreadPool(16);

        while (true) {
            final Socket client;
            try {
                client = calibrationServer.accept();
                client.setTcpNoDelay(true);
            } catch (Exception e) {
                Trace.writeLine("Failed to accept client for calibration", e);
                continue;
            }

            final SocketAddress ep = client.getRemoteSocketAddress();

            executor.execute(new Runnable() {
                public void run() {
                    try {
                        processCalibrationExchange(client, ep);
                    } catch (Exception e) {
                        Trace.writeLine("Failed to process calibration exchange with " + ep, e);
                    } finally {
                        try {
                            client.close();
                        } catch (IOException e) {
                            // Ignore
                        }
                    }
                }
            });
        }
    }

    private void processCalibrationExchange(Socket client, SocketAddress ep) throws IOException {
        InputStream in = client.getInputStream();
        OutputStream out = client.getOutputStream();

        DataOutput w = new LittleEndianDataOutputStream(out);
        DataInput r = new LittleEndianDataInputStream(in);

        UUID uuid = new UUID(r.readLong(), r.readLong());

        // We exchange *ticks* (0.1us intervals)

        // Do some iterations to warm up the TCP connection
        for (int i = 0; i < conf.preCalibrationIters; ++i) {
            w.writeLong(PreciseClock.INSTANCE.now());
            r.readLong();
        }

        // The smaller the network roundtrip, the smaller the range of possible
        // asymmetries of network latencies, the more precisely we compute the
        // clock difference.
        long minLatencyNanos = Long.MAX_VALUE;
        long latenessAtMinLatencyNanos = 0;

        for (int i = 0; i < conf.maxCalibrationIters; ++i) {
            long beforeSend = PreciseClock.INSTANCE.now();
            w.writeLong(beforeSend);
            long clientTime = r.readLong();
            long afterReceive = PreciseClock.INSTANCE.now();

            long latencyNanos = (afterReceive - beforeSend) / 2;
            if(latencyNanos < minLatencyNanos) {
                minLatencyNanos = latencyNanos;
                latenessAtMinLatencyNanos = clientTime - beforeSend - latencyNanos;
            }

            if (i >= conf.minCalibrationIters && latencyNanos < conf.desiredConfidenceRangeMs * 1000000L) {
                break;
            }
        }

        Trace.writeLine("Clock lateness with client " + ep + " (" + uuid + ") is " + latenessAtMinLatencyNanos);
        clientLateness.put(uuid, latenessAtMinLatencyNanos);
    }


    private void flushCalibratedMessages() {
        Thread.currentThread().setPriority(7); // above normal

        while(true) {

            List<Record> snapshot = new ArrayList<Record>(10000);

            for (Map.Entry<UUID, Long> p : clientLateness.entrySet()) {
                UUID client = p.getKey();
                long lateness = p.getValue();

                Queue<Record> q = clientRecords.get(client);

                if(q != null) {
                    snapshot.clear();
                    for(int i = 0; i < 10000; ++i) {
                        Record r = q.poll();
                        if(r == null) {
                            break;
                        }
                        snapshot.add(r.absolutizeTime(lateness));
                    }

                    Trace.writeLine("Dequeued snapshot: " + snapshot.size());

                    numPendingUncalibratedEntries.addAndGet(-snapshot.size());
                    outputQueue.enqueue(snapshot);
                }
            }

            try {
                Thread.sleep(10);
            } catch (InterruptedException e) {}
        }
    }


}
