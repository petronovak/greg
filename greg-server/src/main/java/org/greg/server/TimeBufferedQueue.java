package org.greg.server;

import java.util.ArrayList;
import java.util.Comparator;
import java.util.List;
import java.util.PriorityQueue;

public class TimeBufferedQueue {
    private final long windowSize;
    private final Clock clock;

    private final PriorityQueue<Record> inputQueue;
    private final int maxQueuedRecords;
    private int numRecords = 0;

    private final Object sync = new Object();

    public TimeBufferedQueue(long windowSize, Clock clock, int maxQueuedRecords, Comparator<Record> comparer) {
        this.windowSize = windowSize;
        this.clock = clock;
        this.maxQueuedRecords = maxQueuedRecords;

        this.inputQueue = new PriorityQueue<Record>(10000, comparer);
    }

    public void enqueue(List<Record> entries) {
        synchronized(sync) {
            int toOffer = Math.min(entries.size(), maxQueuedRecords - numRecords);

            for (int i = 0; i < toOffer; ++i) {
                inputQueue.offer(entries.get(i));
            }

            numRecords += toOffer;

            if(entries.size() > toOffer) {
                Trace.writeLine("Calibrated records buffer full - " + (entries.size() - toOffer) + " records dropped");
            }
        }
    }

    public List<Record> dequeue() {
        List<Record> res = new ArrayList<Record>();
        long dueTime = clock.now();

        synchronized (sync) {
            while (true) {
                Record item = inputQueue.peek();
                if (item == null || item.timestamp > (dueTime - windowSize)) break;
                inputQueue.remove();
                numRecords--;
                res.add(item);
            }
        }

        return res;
    }
}
