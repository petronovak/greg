package org.greg.logback.appender;

import ch.qos.logback.classic.spi.ILoggingEvent;
import ch.qos.logback.core.AppenderBase;
import ch.qos.logback.classic.encoder.PatternLayoutEncoder;
import ch.qos.logback.core.Layout;
import org.greg.client.Greg;

/**
 * 
 * @author Yaroslav Derman
 * 
 * e-mail - yaricderman@gmail.com
 *
 */
public class GregLogbackAppender extends AppenderBase<ILoggingEvent> {

    private String server               = System.getProperty("greg.server", "localhost");
    private String port                 = System.getProperty("greg.port", "5676");
    private String calibrationPort      = System.getProperty("greg.calibrationPort", "5677");
    private int flushPeriodMs           = Integer.parseInt(System.getProperty("greg.flushPeriodMs", "1000"));
    private String clientId             = System.getProperty("greg.clientId", "unknown");
    private int maxBufferedRecords      = Integer.parseInt(System.getProperty("greg.maxBufferedRecords", "1000000"));
    private boolean useCompression      = Boolean.parseBoolean(System.getProperty("greg.useCompression", "true"));
    private int calibrationPeriodSec    = Integer.parseInt(System.getProperty("greg.calibrationPeriodSec", "10"));

    private PatternLayoutEncoder encoder = null;

    public GregLogbackAppender() {}

    @Override
    protected void append(ILoggingEvent loggingEvent) {
        Layout<ILoggingEvent> layout = encoder.getLayout();
        
        String msg;
        
        if (layout != null) {
        	msg = layout.doLayout(loggingEvent);
        } else {
            String s = loggingEvent.getThrowableProxy().getMessage();

            if (s != null) {
            	msg = loggingEvent.getFormattedMessage() + s;
            } else {
            	msg = loggingEvent.getFormattedMessage();
            }
        }
        
        Greg.log(msg);
    }

    @Override
    public void start() {
        super.start();
        System.setProperty("greg.server", server);
        System.setProperty("greg.port", port);
        System.setProperty("greg.calibrationPort", calibrationPort);
        System.setProperty("greg.flushPeriodMs", String.valueOf(flushPeriodMs));
        System.setProperty("greg.clientId", clientId);
        System.setProperty("greg.maxBufferedRecords", String.valueOf(maxBufferedRecords));
        System.setProperty("greg.useCompression", String.valueOf(useCompression));
        System.setProperty("greg.calibrationPeriodSec", String.valueOf(calibrationPeriodSec));
    }

    @Override
    public void stop() {
        super.stop();
        try {
            Greg.shutdownAndAwait(1000);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    public String getServer() {
        return server;
    }

    public void setServer(String server) {
        this.server = server;
    }

    public String getPort() {
        return port;
    }

    public void setPort(String port) {
        this.port = port;
    }

    public String getCalibrationPort() {
        return calibrationPort;
    }

    public void setCalibrationPort(String calibrationPort) {
        this.calibrationPort = calibrationPort;
    }

    public int getFlushPeriodMs() {
        return flushPeriodMs;
    }

    public void setFlushPeriodMs(int flushPeriodMs) {
        this.flushPeriodMs = flushPeriodMs;
    }

    public String getClientId() {
        return clientId;
    }

    public void setClientId(String clientId) {
        this.clientId = clientId;
    }

    public int getMaxBufferedRecords() {
        return maxBufferedRecords;
    }

    public void setMaxBufferedRecords(int maxBufferedRecords) {
        this.maxBufferedRecords = maxBufferedRecords;
    }

    public boolean isUseCompression() {
        return useCompression;
    }

    public void setUseCompression(boolean useCompression) {
        this.useCompression = useCompression;
    }

    public int getCalibrationPeriodSec() {
        return calibrationPeriodSec;
    }

    public void setCalibrationPeriodSec(int calibrationPeriodSec) {
        this.calibrationPeriodSec = calibrationPeriodSec;
    }

    public PatternLayoutEncoder getEncoder() {
        return encoder;
    }

    public void setEncoder(PatternLayoutEncoder encoder) {
        this.encoder = encoder;
    }
}